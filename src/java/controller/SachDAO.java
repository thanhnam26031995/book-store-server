package controller;

import java.util.ArrayList;
import model.Sach;

import com.sun.corba.se.pept.transport.Connection;


public interface SachDAO {
	public boolean getConnection();
	public ArrayList<Sach> timkiemtheoten(String ten);
	public ArrayList<Sach> timkiemtheotacgia(String tacgia);
        public Sach timkiemtheoma(int ma);
	
}
