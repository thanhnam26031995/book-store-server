package controller;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Sach;

public class SachDAOIml implements SachDAO {

    java.sql.Connection conn;

    public SachDAOIml() {
        getConnection();
    }

    @Override
    public boolean getConnection() {
        String dbUrl = "jdbc:mysql://localhost:3306/banhang";
        String dbClass = "com.mysql.jdbc.Driver";
        try {
            Class.forName(dbClass);
            conn = DriverManager.getConnection(dbUrl, "root", "");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public ArrayList<Sach> timkiemtheoten(String ten) {
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "SELECT * FROM sach where tensach like ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + ten + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Sach mh = new Sach();
                mh.setMa(rs.getInt("ma"));
                mh.setTensanh(rs.getString("tensach"));
                mh.setAnh(rs.getString("anh"));
                mh.setTheloai(rs.getString("theloai"));
                mh.setNXB(rs.getString("NXB"));
                mh.setNamXB(rs.getInt("namXB"));
                mh.setTacgia(rs.getString("tacgia"));
                mh.setTaiban(rs.getInt("taiban"));
                mh.setMota(rs.getString("mota"));
                mh.setGiaban(rs.getDouble("giaban"));
                list.add(mh);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    @Override
    public ArrayList<Sach> timkiemtheotacgia(String tacgia) {
        ArrayList<Sach> list = new ArrayList<Sach>();
        String sql = "select * from sach where tacgia like ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + tacgia + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Sach mh = new Sach();
                mh.setMa(rs.getInt(1));
                mh.setTensanh(rs.getString(2));
                mh.setAnh(rs.getString(3));
                mh.setTheloai(rs.getString(4));
                mh.setNXB(rs.getString(5));
                mh.setNamXB(rs.getInt(6));
                mh.setTacgia(rs.getString(7));
                mh.setTaiban(rs.getInt(8));
                mh.setMota(rs.getString(9));
                list.add(mh);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    @Override
    public Sach timkiemtheoma(int ma) {
        Sach mh = new Sach();
        String sql = "SELECT * FROM sach where ma= ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, ma);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                mh.setMa(rs.getInt("ma"));
                mh.setTensanh(rs.getString("tensach"));
                mh.setAnh(rs.getString("anh"));
                mh.setTheloai(rs.getString("theloai"));
                mh.setNXB(rs.getString("NXB"));
                mh.setNamXB(rs.getInt("namXB"));
                mh.setTacgia(rs.getString("tacgia"));
                mh.setTaiban(rs.getInt("taiban"));
                mh.setMota(rs.getString("mota"));
                mh.setGiaban(rs.getDouble("giaban"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return mh; //To change body of generated methods, choose Tools | Templates.
    }

}
