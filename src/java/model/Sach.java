package model;

import java.io.Serializable;

public class Sach implements Serializable{
	private int ma;
	private String tensanh;
	private String anh;
	private String theloai;
	private String NXB;
	private int namXB;
	private String tacgia;
	private int taiban;
        private double giaban;
	private String mota;

    public double getGiaban() {
        return giaban;
    }

    public void setGiaban(double giaban) {
        this.giaban = giaban;
    }

	public Sach() {
		// TODO Auto-generated constructor stub
	}

	public int getMa() {
		return ma;
	}

	public void setMa(int ma) {
		this.ma = ma;
	}

	public String getTensanh() {
		return tensanh;
	}

	public void setTensanh(String tensanh) {
		this.tensanh = tensanh;
	}

	public String getAnh() {
		return anh;
	}

	public void setAnh(String anh) {
		this.anh = anh;
	}

	public String getTheloai() {
		return theloai;
	}

	public void setTheloai(String theloai) {
		this.theloai = theloai;
	}

	public String getNXB() {
		return NXB;
	}

	public void setNXB(String nXB) {
		NXB = nXB;
	}

	public int getNamXB() {
		return namXB;
	}

	public void setNamXB(int namXB) {
		this.namXB = namXB;
	}

	public String getTacgia() {
		return tacgia;
	}

	public void setTacgia(String tacgia) {
		this.tacgia = tacgia;
	}

	public int getTaiban() {
		return taiban;
	}

	public void setTaiban(int taiban) {
		this.taiban = taiban;
	}

	public String getMota() {
		return mota;
	}

	public void setMota(String mota) {
		this.mota = mota;
	}

}
