/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookstoreWS;

import controller.SachDAOIml;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Sach;

/**
 * REST Web Service
 *
 * @author Nguyen Thanh Nam
 */
@Path("bookstoreWS")
public class GenericResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

   
    private ArrayList<Sach> list = new ArrayList<>();
    private SachDAOIml dao = new SachDAOIml();

    /**
     * Retrieves representation of an instance of code.sach
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/timkiem/{ten}")
    @Produces("text/xml")
    public String getXml(@PathParam("ten") String ten) {
        ArrayList<Sach> dstimkiem=new ArrayList<>();
        dstimkiem=dao.timkiemtheoten(ten);
        String st = "<DSSach>";
        for (Sach s : dstimkiem ) {
            st += "<Sach>"
                    + "<ma>" + s.getMa() + "</ma>"
                    + "<tensach>" + s.getTensanh() + "</tensach>"
                    + "<anh>" + s.getAnh() + "</anh>"
                    + "<theloai>" + s.getTheloai() + "</theloai>"
                    + "<NXB>" + s.getNXB() + "</NXB>"
                    + "<namXB>" + s.getNamXB() + "</namXB>"
                    + "<tacgia>" + s.getTacgia() + "</tacgia>"
                    + "<taiban>" + s.getTaiban() + "</taiban>"
                    + "<giaban>" + s.getGiaban()+ "</giaban>"
                    + "<mota>" + s.getMota() + "</mota>"
                    + "</Sach>";
        }
        return st + "</DSSach>";
    }
    @GET
    @Path("/timkiem/ma/{ma}")
    @Produces("text/xml")
    public String getXml(@PathParam("ma") int ma) {
        Sach s=new Sach();
        s=dao.timkiemtheoma(ma);
        String st = "<DSSach>";
            st += "<Sach>"
                    + "<ma>" + s.getMa() + "</ma>"
                    + "<tensach>" + s.getTensanh() + "</tensach>"
                    + "<anh>" + s.getAnh() + "</anh>"
                    + "<theloai>" + s.getTheloai() + "</theloai>"
                    + "<NXB>" + s.getNXB() + "</NXB>"
                    + "<namXB>" + s.getNamXB() + "</namXB>"
                    + "<tacgia>" + s.getTacgia() + "</tacgia>"
                    + "<taiban>" + s.getTaiban() + "</taiban>"
                    + "<giaban>" + s.getGiaban()+ "</giaban>"
                    + "<mota>" + s.getMota() + "</mota>"
                    + "</Sach>";
        
        return st + "</DSSach>";
    }

    @GET
    @Path("danhsach")
    @Produces("text/xml")
    public String getXml2() {
        ArrayList<Sach> list = new ArrayList<>();
        list = dao.timkiemtheoten("");
        String st = "<DSSach>";
        for (Sach s : list) {
            st += "<Sach>"
                    + "<ma>" + s.getMa() + "</ma>"
                    + "<tensach>" + s.getTensanh() + "</tensach>"
                    + "<anh>" + s.getAnh() + "</anh>"
                    + "<theloai>" + s.getTheloai() + "</theloai>"
                    + "<NXB>" + s.getNXB() + "</NXB>"
                    + "<namXB>" + s.getNamXB() + "</namXB>"
                    + "<tacgia>" + s.getTacgia() + "</tacgia>"
                    + "<taiban>" + s.getTaiban() + "</taiban>"
                    + "<giaban>" + s.getGiaban()+ "</giaban>"
                    + "<mota>" + s.getMota() + "</mota>"
                    + "</Sach>";
        }
        return st + "</DSSach>";
    }

    @GET
    @Path("getLam")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLam() {
        return "Lam";
    }
    
    @GET
    @Path("getNam")
    @Produces(MediaType.APPLICATION_JSON)
    public String getNam() {
        return "Nam";
    }
    
}
